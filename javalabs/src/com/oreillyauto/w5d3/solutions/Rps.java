package com.oreillyauto.w5d3.solutions;

public enum Rps {
    ROCK(1),
    PAPER(2),
    SCISSORS(3);
    
    private final int id;
    
    Rps(int inId) {
        this.id = inId;
    }
    
    public int getId() {
        return id;
    }
    
    public static String getStatusLiteral(int inId) {
        for (Rps currentRps : Rps.values()) {
            if (currentRps.getId() == inId) {
                return currentRps.name();
            }
        }
        
        return "";
    }
}