package com.oreillyauto.w5d3.solutions;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Exercise1 {

    public static void main(String[] args) {
        try (BufferedReader br = new BufferedReader(new FileReader(new File("numbers.txt")))) {
            String st;
            List<String> values = new ArrayList<String>();
            
            while((st = br.readLine()) != null) {
                values.addAll(Arrays.asList(st.replaceAll("^\"", "")
                        .split("\"?(,|$)(?=(([^\"]*\"){2})*[^\"]*$) *\"?")));
            }
            
            int sum = 0;
            for (String value : values) {
                sum += new Integer(value);
            }
            
            System.out.println(sum);
            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}