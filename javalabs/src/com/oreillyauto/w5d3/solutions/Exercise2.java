package com.oreillyauto.w5d3.solutions;

import java.security.SecureRandom;
import java.util.Random;
import java.util.Scanner;

public class Exercise2 {
    private static final String YOU_WIN = "You Win!";
    private static final String COMPUTER_WINS = "Computer Wins :(";
    private Scanner scanner = new Scanner(System.in);
    
    public Exercise2() {
        try {
            addGameHook();
            playGame("-- Welcome to Rock Paper Scissors! --");    
        } catch (Exception e) {
            System.out.println(getErrorMessage(e));
        }
    }
    
    private String getErrorMessage(Exception e) {
        StringBuilder sb = new StringBuilder();
        sb.append("Sorry! Internal Error! Error message = ");
        String error = ((e==null||e.getMessage()==null) ? "Null Pointer Exception." : (e.getMessage() + "."));
        sb.append(error);
        return sb.toString();
    }

    private void playGame(String message) {
        showMainMenu(message);
        String choice = getInput();
        
        if ("1".equals(choice) || "2".equals(choice)) {
            if ("1".equals(choice)) {
                int playerChoice = getPlayerChoice("");
                int npcChoice = getNpcChoice(1, 3);
                String result = determineWinner(playerChoice, npcChoice);
                playGame(result);
            } else {
                System.exit(0);
            }
        } else {
            playGame("Sorry, please select 1 or 2.");
        }
    }

    private String determineWinner(int playerChoice, int npcChoice) {
        StringBuilder message = new StringBuilder();
        message.append("You Picked:" + Rps.getStatusLiteral(playerChoice) + " ");
        message.append("Computer Picked:" + Rps.getStatusLiteral(npcChoice) + " ");
        
        if (playerChoice == 1 && npcChoice == 2) {
            message.append(COMPUTER_WINS);
        } else if (playerChoice == 1 && npcChoice == 3) {
            message.append(YOU_WIN);
        } else if (playerChoice == 2 && npcChoice == 1) {
            message.append(YOU_WIN);
        } else if (playerChoice == 2 && npcChoice == 3) {
            message.append(COMPUTER_WINS);
        } else if (playerChoice == 3 && npcChoice == 1) {
            message.append(COMPUTER_WINS);
        } else if (playerChoice == 3 && npcChoice == 2) {
            message.append(YOU_WIN);
        } else {
            message.append("Tie! No winner.");
        }
        
        return message.toString();
    }

    private int getNpcChoice(int min, int max) {
        Random random = new SecureRandom();
        random.setSeed(5165165198193419878L);
        return random.nextInt(max - min + 1) + min;
    }

    private int getPlayerChoice(String message) {
        showMessage(message);
        showRockPaperScissorsMenu("");
        String choice = getInput();
        Integer choiceInt = 0;
        
        if ("1".equals(choice) || "2".equals(choice) || "3".equals(choice)) {
            choiceInt = Integer.parseInt(choice);
        } else {
            getPlayerChoice("Sorry, please select 1, 2, or 3.");
        }
        
        return choiceInt;
    }

    private void showRockPaperScissorsMenu(String message) {
        showMessage(message);
        System.out.println("Please Make A Selection:");        
        System.out.println("1. Rock");
        System.out.println("2. Paper");
        System.out.println("3. Scissors");
    }

    private String getInput() {
        String choice = scanner.nextLine();
        return (choice == null) ? "" : choice.trim();
    }

    private void showMainMenu(String message) {
        showMessage(message);
        System.out.println("Please Select A Menu Option:");
        System.out.println("1. Play Rock Paper Scissors");
        System.out.println("2. Quit Game");
    }

    private void showMessage(String message) {
        if (message != null && message.trim().length() > 0) {
            System.out.println(message);
        }
    }

    private void addGameHook() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
           @Override
           public void run() {
               try {
                   scanner.close();
                   System.out.println("Game Shutdown Successfully");
               } catch (Exception e) {
                   /* do nothing */
               } finally {
                   System.out.println("Goodbye!");
               }
           }
        });
    }

    public static void main(String[] args) {
        new Exercise2();
    }

}
