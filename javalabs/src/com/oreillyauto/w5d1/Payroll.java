package com.oreillyauto.w5d1;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class Payroll {

    public static BigDecimal calculateCompanyPayroll(List<OReillyEmployee> employeeList) {
        BigDecimal payroll = new BigDecimal("0.00");

        for (OReillyEmployee emp : employeeList) {
            payroll = payroll.add(emp.getSalary());
        }

        return payroll;
    }

    public static void giveAnnualRaiseForCompany(List<OReillyEmployee> employeeList, BigDecimal amount) {
        for (OReillyEmployee emp : employeeList) {
            emp.setSalary(emp.getSalary().add(amount));
        }
    }

    public static void happyBirthday(OReillyEmployee emp) {
        if (emp != null && emp.getAge() != null) {
            emp.setAge(emp.getAge() + 1);
        }
    }

    public Date getDate() {
        // TODO Auto-generated method stub
        return new Date();
    }

    
    
}
