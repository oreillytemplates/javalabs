package com.oreillyauto.w5d1;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Week 5 Day 1 Lab
 * 
 * - Expected Output - 
 * Total Payroll: 60000.00
 * Manager Payroll: 40000.00
 * Giving A $500 raise to each employee...
 * Total Payroll: 61000.00
 * Manager Payroll: 40500.00
 * Age Check:
 * Name: Jeffery Brannon Age: 30
 * Name: Bob Evans Age: 30
 * Jeffery Had A Birthday!
 * Age Check:
 * Name: Jeffery Brannon Age: 31
 * Name: Bob Evans Age: 30
 * 
 */
public class LabOne {
	private Payroll payroll;
    private List<OReillyEmployee> employeeList = new ArrayList<OReillyEmployee>();
	public static final String DEFAULT_RAISE = "500.00";
	
    public LabOne() {
        payroll = new Payroll();
        OReillyEmployee employeeOne = new OReillyEmployee(1L, 30, "Jeffery Brannon", 
        		new BigDecimal("20000.00"), "Supervisor");
		OReillyEmployee employeeTwo = new OReillyEmployee(2L, 30, "Bob Evans", 
				new BigDecimal("40000.00"), "Manager");
		employeeList.add(employeeOne);
		employeeList.add(employeeTwo);
		
		System.out.println("Total Payroll: " + calculateCompanyPayroll());
		System.out.println("Manager Payroll: " + calculateCompanyPayroll("Manager"));
		
		// Using a public static final default value, give raises 
		System.out.println("Give the default raise to each employee...");
		giveAnnualRaiseForCompany(new BigDecimal(DEFAULT_RAISE));
		
		System.out.println("Total Payroll: " + calculateCompanyPayroll());
		System.out.println("Manager Payroll: " + calculateCompanyPayroll("Manager"));
		
		System.out.println("Age Check:");
		
		printEmployeeNamesAndAges();
		
		Payroll.happyBirthday(employeeOne);
		System.out.println("Jeffery Had A Birthday!");
		
		System.out.println("Age Check:");
		
		printEmployeeNamesAndAges();
    }

	private void printEmployeeNamesAndAges() {
		for (OReillyEmployee emp : employeeList) {
		    System.out.println("Name: " + emp.getName() + " Age: " + emp.getAge());
        }
	}

	private void giveAnnualRaiseForCompany(BigDecimal amount) {
		// call Payroll class
		Payroll.giveAnnualRaiseForCompany(employeeList, amount);
	}

	// Total company payroll
	private BigDecimal calculateCompanyPayroll() {
		// call Payroll class and return the company payroll
		
	    System.out.println(payroll.getDate());
	    
		return Payroll.calculateCompanyPayroll(employeeList);
	}
	
	// Total company payroll by title
	private BigDecimal calculateCompanyPayroll(String title) {
		// call Payroll class
	    List<OReillyEmployee> titleList = new ArrayList<OReillyEmployee>();
	    
	    for (OReillyEmployee empl : employeeList) {
            if (title != null && title.length() > 0) {
                if (title.equalsIgnoreCase(empl.getTitle())) {
                    titleList.add(empl);
                }
            }
        }
	    
	    return Payroll.calculateCompanyPayroll(titleList);
	}
	
    // Main method goes here
	public static void main(String[] args) {
	    new LabOne();
	}
	
	// List Help
	// List<ObjectType> myList = new ArrayList<ObjectType>();
	// * Add to a list
	// ObjectType t = new ObjectType();
	// myList.add(t);
	
}

