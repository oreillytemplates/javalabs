package com.oreillyauto.w5d2.Solution;
/* EXPECTED OUTPUT:
 * Wed Sep 30 13:18:48 CDT 2020 Starting process...
 * 0
 * 1
 * 2
 * 3
 * 4
 * Wed Sep 30 13:18:48 CDT 2020 Process Complete.
 * Wed Sep 30 13:18:48 CDT 2020 Done
 */
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Exercise2 {

    List<Integer> intList = new ArrayList<Integer>();
    
    public Exercise2() {
        logMessage("Starting process...", "%s");
        populateList();
        iterateList();
        logMessage("Process Complete.", "%s");
    }

    private void populateList() {
        for (int i = 0; i < 5; i++) {
            intList.add(i);
        }
    }

    private void iterateList() {
        for (Integer integer : intList) {
            System.out.println(integer);
        }
    }

    public static void main(String[] args) {        
        new Exercise2();
        logStaticMessage("Done", "%s");
    }

    private static void logMessage(String message, String type) {
        System.out.printf(type + " " + (message + "\n") , new Date());
    }
    
    private static void logStaticMessage(String message, String type) {
        System.out.printf(type + " " + (message + "\n") , new Date());
    }
    
}