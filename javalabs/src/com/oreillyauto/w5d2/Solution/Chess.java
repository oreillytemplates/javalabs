package com.oreillyauto.w5d2.Solution;

import java.awt.Point;
import java.util.HashMap;
import java.util.Map;

public class Chess {
    /*
     * Chess Board: 
     *    0  1  2  3  4  5  6  7  
     * 0 BR BN BB BK BQ BB BN BR 
     * 1 BP BP BP BP BP BP BP BP 
     * 2 -- -- -- -- -- -- -- -- 
     * 3 -- -- -- -- -- -- -- -- 
     * 4 -- -- -- -- -- -- -- -- 
     * 5 -- -- -- -- -- -- -- -- 
     * 6 WP WP WP WP WP WP WP WP 
     * 7 WR WN WB WK WQ WB WN WR 
     * 
     */
    public Chess() {
        // Create Chess Pieces
        ChessPiece br = new ChessPiece(ChessPiece.TYPE_ROOK, ChessPiece.COLOR_BLACK);
        ChessPiece bn = new ChessPiece(ChessPiece.TYPE_KNIGHT, ChessPiece.COLOR_BLACK);
        ChessPiece bb = new ChessPiece(ChessPiece.TYPE_BISHOP, ChessPiece.COLOR_BLACK);
        ChessPiece bk = new ChessPiece(ChessPiece.TYPE_KING, ChessPiece.COLOR_BLACK);
        ChessPiece bq = new ChessPiece(ChessPiece.TYPE_QUEEN, ChessPiece.COLOR_BLACK);
        ChessPiece bp = new ChessPiece(ChessPiece.TYPE_PAWN, ChessPiece.COLOR_BLACK);
        ChessPiece wr = new ChessPiece(ChessPiece.TYPE_ROOK, ChessPiece.COLOR_WHITE);
        ChessPiece wn = new ChessPiece(ChessPiece.TYPE_KNIGHT, ChessPiece.COLOR_WHITE);
        ChessPiece wb = new ChessPiece(ChessPiece.TYPE_BISHOP, ChessPiece.COLOR_WHITE);
        ChessPiece wk = new ChessPiece(ChessPiece.TYPE_KING, ChessPiece.COLOR_WHITE);
        ChessPiece wq = new ChessPiece(ChessPiece.TYPE_QUEEN, ChessPiece.COLOR_WHITE);
        ChessPiece wp = new ChessPiece(ChessPiece.TYPE_PAWN, ChessPiece.COLOR_WHITE);
        
        // Build the Map
        Map<Point, ChessPiece> board = new HashMap<Point, ChessPiece>();        
        
        // Add Chess Pieces To The Map
        board.put(new Point(0,0), br);
        board.put(new Point(1,0), bn);
        board.put(new Point(2,0), bb);
        board.put(new Point(3,0), bk);
        board.put(new Point(4,0), bq);
        board.put(new Point(5,0), bb);
        board.put(new Point(6,0), bn);
        board.put(new Point(7,0), br);
        
        for (int i = 0; i < 8; i++) {
            board.put(new Point(i,1), bp);
            board.put(new Point(i,6), wp);
        }
        
        board.put(new Point(0,7), wr);
        board.put(new Point(1,7), wn);
        board.put(new Point(2,7), wb);
        board.put(new Point(3,7), wk);
        board.put(new Point(4,7), wq);
        board.put(new Point(5,7), wb);
        board.put(new Point(6,7), wn);
        board.put(new Point(7,7), wr);
        
        StringBuilder sb = new StringBuilder();
        
        sb.append("   0  1  2  3  4  5  6  7\n");
        
        for (int i = 0; i < 8; i++) {
            sb.append(i + " ");
            
            for (int j=0; j < 8; j++) {
                ChessPiece piece = board.get(new Point(j,i));
                
                if (piece == null) {
                    sb.append("--");
                } else {
                    sb.append(board.get(new Point(j,i)).getAbbreviation());
                }
                
                sb.append((j==7) ? "" : " ");
            }
            
            sb.append("\n");
        }
        
        System.out.println(sb);
    }

    public static void main(String[] args) {
        new Chess();
    }

}
