package com.oreillyauto.w5d2.problems;

import java.awt.Point;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * EXPECTED OUTPUT:
 * 
 * Largest int: 5
 * Calculating 5.0 plus 2.0: 7.0
 * Counting to 'n' (5): 1 2 3 4 5 
 * Checking if 101 is even: false
 * Replacing all n's with t's in the word banana: batata
 * 37 Degrees Fahrenheit to Celsius: 2.778
 * 1000 inches to meters: 25.40
 * Sum of digits (12345): 15
 * Chess Board: 
 * 0  1  2  3  4  5  6  7  
 * 0 BR BN BB BK BQ BB BN BR 
 * 1 BP BP BP BP BP BP BP BP 
 * 2 -- -- -- -- -- -- -- -- 
 * 3 -- -- -- -- -- -- -- -- 
 * 4 -- -- -- -- -- -- -- -- 
 * 5 -- -- -- -- -- -- -- -- 
 * 6 WP WP WP WP WP WP WP WP 
 * 7 WR WN WB WK WQ WB WN WR 
 * 
 */
public class Exercise1 {

	/**
	 * The Class Constructor points to methods in the class. The methods, 
	 * however, are not complete. Your task is to complete the methods below
	 * and compare your output to the expected output.
	 */
	public Exercise1() {	
		// Find the largest int in the given array
		System.out.println("Largest int: " + greatest(new int[] { 1, 2, 3, 4, 5 }));

		// Given 2 values and an operand, build a simple calculator to find the solution
		System.out.println("Calculating 5.0 plus 2.0: " + calculator(5.0, "+", 2.0));

		// Count from 1 to n (returns a String so we will add a new line)
		System.out.print("Counting to 'n' (5): " + countTo(5));
		System.out.println("");

		// Check if a number is even or odd
		System.out.println("Checking if 101 is even: " + isEven(101));

		// Character replacement: Given a word, replace a certain letter with another letter
		System.out.println("Replacing all n's with t's in the word banana: " + replaceCharacters("banana", 'n', 't'));

		// Convert a given temperature from Fahrenheit to Celsius
		// To convert temperatures in degrees Fahrenheit to Celsius, subtract 32 and multiply by .5556 (or 5/9).
		System.out.println("37 Degrees Fahrenheit to Celsius: " + fahrenheitToCelsius(new BigDecimal(37)));

		// Convert a given distance from inches to meters
		// Divide the length value (inches) by 39.37
		System.out.println("1000 inches to meters: " + inchesToMeters(1000));

		// Digit Summation
		// Given any int, find the sum of the individual digits
		System.out.println("Sum of digits (12345): " + sumOfDigits(12345));

		// Build a Chess Board layout
		System.out.println("Chess Board: " + buildChessBoard());
	}

	/**
	 * Using Objects and loops/iterations, build a chessboard in the console that
	 * looks like the one below. I have created a ChessPiece class if you want to
	 * use that to get you started. Do not simply print the chessboard to the
	 * console manually.
	 * 
	 * HINTS: 
	 * Let's place a black rook at position 0,0 using a Map that has a Point(x,y)
	 * position as its key, and a ChessPiece as the value.
	 * 
	 * First, let's build the Point.
	 * - A java.awt.Point holds x,y coordinates.
	 * --  example: Point point = new Point(0,0);
	 * 
	 * Next, let's build a ChessPiece (Rook)
	 * - ChessPiece.java holds type and color    
	 * --  example: ChessPiece rook = new ChessPiece(ChessPiece.TYPE_ROOK, ChessPiece.COLOR_BLACK);
	 * 
	 * Finally, let's place the rook at position 0,0 on our board (map).
	 * - A java.util.HashMap is a great data structure because it holds a key and 
	 *   a value. The key could represent the position (x,y coordinate) on the board 
	 *   while the value could be a ChessPiece Object that represents a chesspiece 
	 *   on the board with a type and color.
	 *   
	 *   Map<Point, ChessPiece> board = new HashMap<Point, ChessPiece>();
	 *   board.put(point, rook); 
	 * 
	 * @return
	 */
	//    0  1  2  3  4  5  6  7 
	// 0 BR BN BB BK BQ BB BN BR
	// 1 BP BP BP BP BP BP BP BP
	// 2 -- -- -- -- -- -- -- --
	// 3 -- -- -- -- -- -- -- --
	// 4 -- -- -- -- -- -- -- --
	// 5 -- -- -- -- -- -- -- --
	// 6 WP WP WP WP WP WP WP WP
	// 7 WR WN WB WK WQ WB WN WR
	private String buildChessBoard() {		
		// Create Chess Pieces
		ChessPiece br = new ChessPiece(ChessPiece.TYPE_ROOK, ChessPiece.COLOR_BLACK);
		ChessPiece bn = new ChessPiece(ChessPiece.TYPE_KNIGHT, ChessPiece.COLOR_BLACK);
		ChessPiece bb = new ChessPiece(ChessPiece.TYPE_BISHOP, ChessPiece.COLOR_BLACK);
		ChessPiece bk = new ChessPiece(ChessPiece.TYPE_KING, ChessPiece.COLOR_BLACK);
		ChessPiece bq = new ChessPiece(ChessPiece.TYPE_QUEEN, ChessPiece.COLOR_BLACK);
		ChessPiece bp = new ChessPiece(ChessPiece.TYPE_PAWN, ChessPiece.COLOR_BLACK);
		ChessPiece wr = new ChessPiece(ChessPiece.TYPE_ROOK, ChessPiece.COLOR_WHITE);
		ChessPiece wn = new ChessPiece(ChessPiece.TYPE_KNIGHT, ChessPiece.COLOR_WHITE);
		ChessPiece wb = new ChessPiece(ChessPiece.TYPE_BISHOP, ChessPiece.COLOR_WHITE);
		ChessPiece wk = new ChessPiece(ChessPiece.TYPE_KING, ChessPiece.COLOR_WHITE);
		ChessPiece wq = new ChessPiece(ChessPiece.TYPE_QUEEN, ChessPiece.COLOR_WHITE);
		ChessPiece wp = new ChessPiece(ChessPiece.TYPE_PAWN, ChessPiece.COLOR_WHITE);
		
		// Build map
		Map<Point, ChessPiece> board = new HashMap<Point, ChessPiece>();
		
		// add chess pieces to map
		board.put(new Point(0, 0), br);
		board.put(new Point(1, 0), bn);
		board.put(new Point(2, 0), bb);
		board.put(new Point(3, 0), bk);
		board.put(new Point(4, 0), bq);
		board.put(new Point(5, 0), bb);
		board.put(new Point(6, 0), bn);
		board.put(new Point(7, 0), br);
		
		for (int i = 0; i < 8; i++) {
			board.put(new Point(i, 1), bp);
			board.put(new Point(i, 6), wp);
		}
		
		board.put(new Point(0, 7), wr);
		board.put(new Point(1, 7), wn);
		board.put(new Point(2, 7), wb);
		board.put(new Point(3, 7), wk);
		board.put(new Point(4, 7), wq);
		board.put(new Point(5, 7), wb);
		board.put(new Point(6, 7), wn);
		board.put(new Point(7, 7), wr);

		//iterate i, j and print the map
		StringBuilder sb = new StringBuilder("\n");
		sb.append("  0  1  2  3  4  5  6  7");
		sb.append("\n");
		
		for (int i = 0; i < 8; i++) {
			sb.append(i);
			sb.append(" ");
			
			for (int j = 0; j < 8; j++) {
				ChessPiece piece = board.get(new Point(j, i));
				
				if (piece == null) {
					sb.append("--");
				} else {
					sb.append(board.get(new Point(j, i)).getAbbreviation());
				}
				
				sb.append(j == 7 ? "" : " ");
			}
			
			sb.append("\n");
		}

		return sb.toString();
	}

	/**
	 * sumOfDigits
	 * Given any int, find the sum of the individual digits
	 * @param i
	 * @return
	 */
	private int sumOfDigits(int i) {
		int sum = 0;
		
		for (String element : String.valueOf(i).split("")) {
			sum += Integer.parseInt(element);
		}
		
		return sum;
	}

	/**
	 * inchesToMeters (SimpleQueries Format:  ##.##)
	 * Divide the length value (inches) by 39.37
	 * Convert a given distance from inches to meters
	 * Optional Strategy:
	 *   Can you solve with <BigDecimal>.divide(BigDecimal divisor, int scale, int roundingMode)?
	 * @param i
	 * @return
	 */
	private BigDecimal inchesToMeters(int inches) {
		return new BigDecimal(inches).divide(new BigDecimal("39.37"), 2, RoundingMode.HALF_UP);
	}

	/**
	 * fahrenheitToCelsius 
	 * Write a Java method to convert temperature from Fahrenheit to Celsius
	 * 
	 * To convert temperatures in degrees Fahrenheit to Celsius, subtract 32 
	 * and multiply by .5556 (or 5/9).
	 * 
	 * @param fahrenheit
	 * @return
	 */
	private String fahrenheitToCelsius(BigDecimal fahrenheit) {
		//float celsius = (fahrenheit - 32) * 5 / 9;
		fahrenheit = fahrenheit.subtract(new BigDecimal("32"));
		fahrenheit = fahrenheit.multiply(new BigDecimal("5"));
		fahrenheit = fahrenheit.divide(new BigDecimal("9"), 3, RoundingMode.HALF_UP);
		return fahrenheit.toString();
	}

	/**
	 * greatest
	 * Find the largest int in the given array
	 * @param ints
	 * @return
	 */
	public static int greatest(int[] ints) {
		Arrays.sort(ints);
		return ints[ints.length - 1];
	}

	/**
	 * calculator
	 * Given 2 values and an operand, build a simple calculator to find the solution
	 * Solve with a switch statement.
	 * @param x
	 * @param operator
	 * @param y
	 * @return
	 */
	public static double calculator(double x, String operator, double y) {
		double value = 0d;
		if ("+".equals(operator)) {
			value = x + y;
		}
		return value;
	}

	/**
	 * countTo 
	 * Count to "n" without any new line characters. Append your iterations
	 * to the StringBuilder Object Make sure there is a space after each number.
	 * 
	 * @param n
	 * @return
	 */
	public static String countTo(int n) {
		StringBuilder sb = new StringBuilder();
		for (int i = 1; i <= n; i++) {
			sb.append(i).append(" ");
		}
		return sb.toString();
	}

	/**
	 * isEven
	 * Check if a number is even or odd
	 * returns a String. String.valueOf()?, .toString()?
	 * @param i
	 * @return
	 */
	private String isEven(int i) {
		return i % 2 == 0 ? "true" : "false";
	}

	/**
	 * replaceCharacters
	 * Character replacement: Given a word, replace a certain letter with another letter
	 * @param str
	 * @param old
	 * @param newChr
	 * @return
	 */
	public static String replaceCharacters(String str, char old, char newChr) {
		return str.replace(old, newChr);
	}

	/**
	 * main
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		new Exercise1();
	}
}
